import React,{Component} from "react";
import {Header, Input} from "../components";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {MAIN} from "../utils/pathContants";
import {login} from "../actions/authActions";

class LoginPage extends Component{

    state = {
        password:'',
        email:'',
        error:null
    };

    onChange = (event) =>{
        const {name,value} = event.target;
        this.setState({[name]:value, error:null});
    };

    onLogin = () => {
        const {password,email} = this.state;
        const user = this.props.users.find((u) => u.email === email && u.password === password);
        if(user){
            this.props.dispatch(login(user));
            this.props.history.push(MAIN);
        }else{
            this.setState({error: "Bad e-mail or password.", password:''});
        }
    };

    render (){
        const {password,email,error} = this.state;
        return(
            <div className="container">
                <Header />
                <h2 className="page-header">
                    Login Page
                </h2>
                <Input title="Email" type="email" name="email" value={email} onChange={this.onChange}/>
                <Input title="Password" type="password" name="password" value={password} error={error} onChange={this.onChange}/>
                <button className="btn btn-primary basic-margin" onClick={this.onLogin} >Login</button>
            </div>
        );
    }
}

const mapStateToProps = ({users}) =>{
    return {users}
};

export default withRouter(connect(mapStateToProps)(LoginPage));