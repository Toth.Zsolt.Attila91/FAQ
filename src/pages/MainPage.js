import React,{Component} from "react";
import {withRouter} from "react-router-dom";
import * as paths from "../utils/pathContants";
import {connect} from "react-redux";
import {Header} from "../components";
import {topics} from "../utils/topics";

class MainPage extends Component{
    onQuestionClick (id){
      this.props.history.push(`${paths.QUESTION}/${id}`);
    }

    onTopicClick (topic){

    }

    renderQuestion = (question) => {
        return (
            <li key={question.id} className="list-group-item">
                <div>
                    <p className="col-md-2"><span className="badge" >
                        {question.topic && <div>Topic:{question.topic}</div>}
                        {question.answerCounter && <span>Answers: {question.answerCounter}</span>}
                        </span>
                       </p>
                    <button onClick={() => this.onQuestionClick(question.id)}>Go to question</button>
                    <b className="col-md-8">{question.title}</b><br/>
                </div>
            </li>
        );
    };

    render(){
        this.topics=topics;
        return (
            <div className="container">
                <Header/>
                <h2 className="page-header">
                    Main Page
                </h2>
                <ul>
                    {this.topics.map((e, key) => {
                        return <button key={key} value={e.value} onClick = { () => this.onTopicClick(this.value)}><option>{e.name}</option></button>;
                    })}
                </ul>
                <br />
                <ul className="list-group">
                    {this.props.questions.map(this.renderQuestion)}
                    <br />
                </ul>
            </div>
        );
    }
}

const mapStateToProps = ({questions}) => {
    return {questions};
};

export default withRouter(connect(mapStateToProps)(MainPage));