import React,{Component} from "react";
import {Header, Input} from "../components";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {registerUser} from "../actions/userActions";
import {MAIN} from "../utils/pathContants";

class RegisterPage extends Component{

    state = {
        name:'',
        password:'',
        email:'',
        error:null
    };

    onChange = (event) =>{
        const {name,value} = event.target;
        let newState = {[name]:value};
        if (name === "email"){
            newState.error = null;
        }
        this.setState(newState);
    };

    onRegister = () => {
        const {name,password,email} = this.state;
        const user = this.props.users.find((u) => u.email === email);
        if(user){
            this.setState({error: "E-mail already registered"});
        }else{
            this.props.dispatch(registerUser(name,email,password));
            this.props.history.push(MAIN);
        }
    };

    render (){
        const {name,password,email,error} = this.state;
        return(
            <div className="container">
                <Header />
                <h2 className="page-header">
                    Register
                </h2>
                <Input title="Name" type="text" name="name" value={name} onChange={this.onChange}/>
                <Input title="Email" type="email" name="email" value={email} error={error} onChange={this.onChange}/>
                <Input title="Password" type="password" name="password" value={password} onChange={this.onChange}/>
                <button onClick={this.onRegister}>Register</button>
            </div>
        );
    }
}

const mapStateToProps = ({users}) => {
  return {users}
};

export default withRouter(connect(mapStateToProps)(RegisterPage));