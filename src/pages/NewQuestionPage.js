import React,{Component} from "react";
import {withRouter} from "react-router-dom";
import * as paths from "../utils/pathContants";
import {connect} from "react-redux";
import {addQuestion} from "../actions/questionActions";
import {Header, Input, TopicDropdown} from "../components";

class NewQuestionPage extends Component{

    state = {
        title: '',
        text: '',
        topic: null,
        answerCounter: null
    };

    onAddQuestionClick = () => {
        this.props.addQuestion(this.state.title,this.state.text,this.props.auth.name,this.state.topic, this.state.answerCounter);
        this.props.history.push(paths.MAIN);
    };

    render(){
        return (
            <div className="container">
                <Header />
                <h2 className="page-header">
                    New Question
                </h2>
                <div>
                    <Input
                        title="Title"
                        onChange={(event)=>{
                            this.setState({title: event.target.value})
                        }}
                        value={this.state.title}
                        type="text"
                    />
                </div>
                <div>
                    <Input
                        title="Question"
                        onChange={(event)=>{
                            this.setState({text: event.target.value})
                        }}
                        value={this.state.text}
                        type="text"
                    />
                </div>

               <div>
                    <TopicDropdown
                        title="Topic"
                        onChange={(event)=>{
                           this.setState({topic: event.target.value})
                        }}
                        value={this.state.topic || ''}
                    />
                </div>

                <button  className="btn btn-primary basic-margin" onClick={this.onAddQuestionClick}>Add question</button>
            </div>
        );
    }
}

const mapStateToProps = ({auth}) =>{
    return {auth};
};

export default withRouter(connect(mapStateToProps,{addQuestion})(NewQuestionPage));