import React,{Component} from "react";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import * as paths from "../utils/pathContants";
import {addAnswer} from "../actions";
import {Header, Input} from "../components";

class QuestionPage extends Component{

    state = {
        text:''
    };


    addAnswerOnClick = ()=>{
        if (this.state.text === '' ){
            window.alert("You have no answer text");
            this.setState({text: ''});
        }else{
            this.props.question.answerCounter = this.props.question.answers.length +1;
            this.props.dispatch(addAnswer(this.state.text,this.props.question.id,this.props.auth.name, this.props.question.answerCounter));
            this.setState({text: ''});
        }
    };

    render(){
        if (!this.props.question){
            return (<Redirect to={paths.MAIN}/>);
        }
        return (
            <div className="container">
                <Header/>
                <div  className="box">

                    <p className="col-md-2">Topic:{this.props.question.topic}</p>


                    <br />
                    <h3>{this.props.question.title}</h3>
                    <p className="label label-default">Asked By: {this.props.question.owner} </p>

                    <p className= "basic-margin">{this.props.question.text}</p>
                </div>
                {this.props.auth &&
                    <React.Fragment>
                        <Input
                            title="Answer"
                            onChange={(event)=>{
                                this.setState({text: event.target.value})
                            }}
                            value={this.state.text}
                            type="text"
                         />
                        <button className="btn btn-primary basic-margin" onClick={this.addAnswerOnClick}>Add answer</button>
                        <br />
                    </React.Fragment>
                }
                <ul className="list-group"> {this.props.question.answers.map(answer =>
                    <li key={answer.id} className="list-group-item">{answer.comment} -
                    <p className="label label-default">Answered By: {answer.owner}</p></li>
                )}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = ({questions,auth}, props) => {
    const id = props.match.params.id;
    const question = questions.find((q) => q.id === id);
    return {question,auth};
};

export default connect(mapStateToProps)(QuestionPage);