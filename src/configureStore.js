import {applyMiddleware, createStore} from "redux";
import {persistStore} from "redux-persist";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import {composeWithDevTools} from "redux-devtools-extension";
import reducers from "./reducers";
import {createSelector} from "reselect";

const logger = createLogger();

const middleware = [ thunk ];

if (process.env.NODE_ENV === 'development') {
    middleware.push(logger);
}

export default () => {
    const store = createStore (
        reducers,
        composeWithDevTools(applyMiddleware(...middleware)),
    );
    const persistor = persistStore(store);
    return { persistor, store };
}

