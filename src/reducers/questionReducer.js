import uuid from "uuid/v1";
import {ADD_ANSWER, ADD_QUESTION, MODIFY_ANSWER, MODIFY_QUESTION} from "../actions/types";

export default function questionReducer (state = [],action){
    switch (action.type) {
        case ADD_QUESTION:
            return [...state,{
                id:uuid(),
                text:action.text,
                answers:[{ answerText: null, likeCounter: action.likeCounter }],
                title: action.title,
                owner: action.owner,
                topic: action.topic,
                answerCounter: action.answerCounter
            }];

        case MODIFY_QUESTION:
            return [...state,{
                text:action.text
            }];

        case ADD_ANSWER:
            let newState = JSON.parse(JSON.stringify(state));
            let question = newState.find((q) => q.id === action.questionId);
            const answers = [{ comment: action.comment, id: uuid(), owner:action.owner, answerCounter:action.answerCounter }, ...question.answers];
            question.answers = answers;
            return newState;

        case MODIFY_ANSWER:
            return [...state,{
                answers: [...state, {
                    comment:action.text
                }]
            }];

    }
    return state;
}


