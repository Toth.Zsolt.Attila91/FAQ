import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import questions from './questionReducer';
import users from "./userReducer";
import auth from "./authReducer";

const config = {
    key: 'root',
    debug: process.env.NODE_ENV === 'development',
    storage,
    whitelist: ['questions', 'users', 'auth'],
};

export default persistCombineReducers(config, {
    questions,
    users,
    auth
});