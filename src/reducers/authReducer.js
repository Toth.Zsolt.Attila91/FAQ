import {LOGIN, LOGOUT, REGISTER_USER} from "../actions/types";


const initialState = null;

export default function (state = initialState,action){
    switch (action.type){
        case REGISTER_USER:
        case LOGIN:
            return action.user;
        case LOGOUT:
            return initialState;
    }
    return state;
}


