import {ADD_QUESTION} from "./types";

export const addQuestion = (title,text,owner,topic,answerCounter) =>{
    return {
        type: ADD_QUESTION,
        title,
        text,
        owner,
        topic,
        answerCounter
    };
};
