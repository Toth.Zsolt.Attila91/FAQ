export const ADD_QUESTION = "ADD_QUESTION";
export const MODIFY_QUESTION = "MODIFY_QUESTION";
export const ADD_ANSWER = "ADD_ANSWER";
export const MODIFY_ANSWER = "MODIFY_ANSWER";
export const REGISTER_USER = "REGISTER_USER";
export const LOGIN= "LOGIN";
export const LOGOUT= "LOGOUT";