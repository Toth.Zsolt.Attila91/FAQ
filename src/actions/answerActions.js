import {ADD_ANSWER} from "./types";

export const addAnswer = (comment, questionId,owner) =>{
    return {
        type: ADD_ANSWER,
        comment,
        questionId,
        owner
    };
};
