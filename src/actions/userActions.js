import {REGISTER_USER} from "./types";

export const registerUser = (name, email, password) =>{
    return {
        type: REGISTER_USER,
        user:{
            name,
            email,
            password
        }
    };
};

