import React from "react";

export const Input = ({title,error, ...rest}) => {
    return (
        <div className= "basic-margin">
            {title}: <br />
            <input {...rest}/>
            {error && <div>{error}</div>}
        </div>
    );
};