import React from "react";
import {topics} from "../utils/topics";

export const TopicDropdown = ({title,...rest}) => {
    this.topics=topics;
    return (

        <div>
            {title}: <br />
            <select title="Topic" {...rest}>
                {this.topics.map((topic, key) => {
                    return <option key={key} value={topic.value}>{topic.name}</option>;
                })}
            </select>
        </div>
    );
};