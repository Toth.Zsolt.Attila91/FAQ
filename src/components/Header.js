import React, {Component} from "react";
import {Link} from "react-router-dom";
import {NEW_QUESTION, REGISTER, LOGIN,MAIN} from "../utils/pathContants";
import {connect} from "react-redux";
import {logout} from "../actions/authActions";

class Header extends Component{
    render (){
        const {auth} = this.props;
        return(
            <div className="container">
                <header className="App-header">
                    <h1 className="App-title">Welcome to My FAQ</h1>
                    <ul className="nav navbar-nav navbar-right">
                        <li><Link to={MAIN}>Main Page</Link></li>
                        <li>{auth && <Link to={NEW_QUESTION}>New Question</Link>}</li>
                        <li>{!auth && <Link to={REGISTER}>Register</Link>}</li>
                        <li>{!auth && <Link to={LOGIN}>Login</Link>}</li>
                        <li> {auth && <Link to={MAIN} onClick={()=>{this.props.dispatch(logout())}}>Logout</Link>}</li>
                        <li>{auth && <div>Logged In: {auth.name}</div>}</li>
                    </ul>
                </header>
            </div>
        );
    }
}

const mapStateToProps = ({auth}) =>{
    return {auth};
};

export default connect(mapStateToProps)(Header);

