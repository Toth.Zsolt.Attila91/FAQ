import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { PersistGate } from "redux-persist/lib/integration/react";
import { Provider } from "react-redux";
import configureStore from "./configureStore";


const { persistor, store } = configureStore();


ReactDOM.render(
    <PersistGate persistor = {persistor}>
        <Provider store = {store}>
            <App />
        </Provider>
    </PersistGate>
    , document.getElementById('root'));

registerServiceWorker();
