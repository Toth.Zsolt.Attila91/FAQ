export const MAIN = "/";
export const QUESTION = "/question";
export const NEW_QUESTION = "/newquestion";
export const REGISTER = "/register";
export const LOGIN = "/login";
export const TOPIC = "/topic";
