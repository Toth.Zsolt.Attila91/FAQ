
export const topics = [
    {value: null, name: "No Topic"},
    {value: "Informatics", name: "Informatics"},
    {value: "Study", name: "Study"},
    {value: "Life-science", name: "Life-science"},
    {value: "History", name: "History"},
];