import React, { Component } from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
    Route
} from 'react-router-dom';

import * as paths from "./utils/pathContants";
import QuestionPage from "./pages/QuestionPage";
import MainPage from "./pages/MainPage";
import NewQuestionPage from "./pages/NewQuestionPage";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import {connect} from "react-redux";


class App extends Component {
    constructor (){
        super();
    }
    render() {
        if (this.props.auth){
            return (
                <div className="App">
                    <Router>
                        <Switch>
                            <Route path={paths.MAIN} component={MainPage} exact />
                            <Route path={`${paths.QUESTION}/:id`} component={QuestionPage} exact />
                            <Route path={paths.NEW_QUESTION} component={NewQuestionPage} exact />
                            <Redirect to={paths.MAIN}/>
                        </Switch>
                    </Router>
                </div>
            );
        }
        return (
            <div className="App">
                <Router>
                        <Switch>
                            <Route path={paths.MAIN} component={MainPage} exact />
                            <Route path={`${paths.QUESTION}/:id`} component={QuestionPage} exact />
                            <Route path={paths.REGISTER} component={RegisterPage} exact />
                            <Route path={paths.LOGIN} component={LoginPage} exact />
                            <Redirect to={paths.MAIN}/>
                        </Switch>
                </Router>
            </div>
        );
    }
}

const mapStateToProps = ({auth})=>{
    return {auth};
};

export default connect(mapStateToProps)(App);
